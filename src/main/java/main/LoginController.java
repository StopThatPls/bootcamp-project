package main;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.validation.Valid;

import javax.persistence.RollbackException;
import org.hibernate.validator.internal.util.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import main.UserService;
import main.exceptions.NotFoundException;
import main.User;
import main.UserRepository;

@Controller
public class LoginController {
	
	@Autowired
	private UserService userService;
	

	@RequestMapping(value={"/login"}, method = RequestMethod.GET)
	public ModelAndView login(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}

	@RequestMapping(value="/registration", method = RequestMethod.GET)
	public ModelAndView registration(){
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		modelAndView.addObject("user", user);
		modelAndView.setViewName("registration");
		return modelAndView;
	}
	
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		Optional<User> userExists = userService.findUserByEmail(user.getEmail());
		if (userExists.isPresent()) {
			bindingResult
					.rejectValue("email", "error.user",
							"There is already a user registered with the email provided");
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("registration");
			return modelAndView;
		} else {
			userService.saveUser(user);
			modelAndView.addObject("successMessage", "User has been registered successfully");
			modelAndView.addObject("user", new User());
			modelAndView.setViewName("registration");
			return new ModelAndView("redirect:/login");
		}
	}
	
	@RequestMapping(value="/admin/home", method = RequestMethod.GET)
	public ModelAndView home(){
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Optional<User> userSearch = userService.findUserByEmail(auth.getName());
		if (userSearch.isPresent()) {
			User user = userSearch.get();
			modelAndView.addObject("userName", "Welcome " + user.getName() + " " +  " (" + user.getEmail() + ")");
			modelAndView.addObject("adminMessage","Content Available Only for Users with Admin Role");
			modelAndView.setViewName("admin/home");
			return modelAndView;
		} else {
			throw new NotFoundException("User not found");
		}
	}
	
	
	
}




//	@Controller 
//	@RequestMapping(path="/user") 
//	public class LoginController {
//		@Autowired
//		
//		private UserRepository uRepository;
//		
//		@RequestMapping(value = "/login", method = RequestMethod.GET)
//		  public String init(Model model) {
//		    model.addAttribute("user", new User());
//		    return "login";
//		}
//		
//		@PostMapping(value = "/login")
//		  public String submit(Model model, @ModelAttribute("user") User user) {
//		    if (user != null && user.getName() != null && user.getPassword() != null) {
//		    	if (uRepository.findByNameAndPassword(user.getName(), user.getPassword()) != null) {
//		    		model.addAttribute("msg", user.getName());
//		        	return "redirect:../";
//		      } else {
//		    	  model.addAttribute("error", "Wrong username or password");
//		    	  return "redirect:../";
//		    	  //return "login";
//		      }
//		    } else {
//		    	model.addAttribute("error", "Error occurred while proccesing");
//		    	return "redirect:../";
//		    	//return "login";
//		    }
//		}
//}


