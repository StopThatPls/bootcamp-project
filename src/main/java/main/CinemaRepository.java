package main;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import main.Cinema;

public interface CinemaRepository extends CrudRepository<Cinema, Long> {
	@Transactional
	@Modifying
    @Query("update Cinema c set c.name = ?1, c.city = ?2,c.address = ?3 where c.id = ?4")
    void updateCinema(String name, String city, String address, long id);
	
	public List <Cinema> findByCity(String cityname);
	
	@Transactional
	@Modifying
    @Query("update Cinema c set c.active = ?1 where c.id = ?2")
    void updateCinemaActive(boolean active, long id);
	
	
}
