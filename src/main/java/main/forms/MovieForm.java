package main.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class MovieForm {
	
	
	
	@NotNull(message="Cannot be empty")
	@Pattern(message = "Only letters and numbers allowed",regexp = "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$")
	private String name;
	
	@NotNull(message="Cannot be empty")
	@Pattern(message = "Only letters and numbers allowed",regexp = "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$")
	private String description;
	

	@NotNull(message="Cannot be empty")
    @Pattern(message = "Only full url allowed",regexp = "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)")
	private String trailerLink;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTrailerLink() {
		return trailerLink;
	}

	public void setTrailerLink(String trailerLink) {
		this.trailerLink = trailerLink;
	}
	
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
