package main.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class CinemaForm {
	
	
	
	@NotNull(message="Cannot be empty")
    @Pattern(message = "Only letters allowed",regexp = "^[A-Za-z _]*[A-Za-z][A-Za-z _]*$")
	private String name;
	
	@NotNull(message="Cannot be empty")
    @Pattern(message = "Only letters allowed",regexp = "^[A-Za-z _]*[A-Za-z][A-Za-z _]*$")
	private String city;
	
	@NotNull(message="Cannot be empty")
    @Pattern(message = "Only letters and numbers allowed",regexp = "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$")
	private String address;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    
    

    public String toString() {
        return "Person(Name: " + this.name + ", Age: )";
    }
}
