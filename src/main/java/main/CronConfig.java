package main;

import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableAsync
@EnableScheduling
public class CronConfig {
	@Autowired
	private PurchaseRepository purchaseRepository;
	@Autowired
	private SeatReservationRepository seatReservationRepository;
	@Autowired
	private ScheduleRepository scheduleRepository;;

	@Value("${main.purchase.timeout}")
	private Long purchaseTimeoutMinutes;
	@Value("${main.purchase.stopsalesafter}")
	private Long stopSalesMinutes;

	@Bean
    public Executor taskExecutor() {
        return new SimpleAsyncTaskExecutor();
    }
	
	@Scheduled(cron="0 * * * * *")
	public void clearUnpaidPurchases() {
		List<Purchase> unpaidPurchases = purchaseRepository.findByStatus('r');
		for (Purchase purchase : unpaidPurchases) {
			if (purchase.getLastActionDateTime().before(new Timestamp(System.currentTimeMillis() - purchaseTimeoutMinutes * 60 * 1000))) {
				for (SeatReservation reservation: seatReservationRepository.findByPurchase(purchase)) {
					seatReservationRepository.delete(reservation);
				}
				purchaseRepository.delete(purchase);
			}
		}
	}

	@Scheduled(cron="0 * * * * *")
	public void disablePastShowtimes() {
		List<CinemaSchedule> schedules = scheduleRepository.findByTimeFromLessThanAndActive(new Timestamp(System.currentTimeMillis() - stopSalesMinutes * 60 * 1000), true);
		for (CinemaSchedule schedule : schedules) {
			schedule.setActive(false);
			scheduleRepository.save(schedule);
		}
	}

}
