package main;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

import javax.servlet.http.HttpServletRequest;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;

public class UserIp{
	
	private InetAddress ipAddress;
    private String city;
	
	
	
	public UserIp(InetAddress ip, String city) {
		this.ipAddress = ip;
		this.city = city;
	}
	public UserIp() {
	
	}
		
	
	
	public InetAddress getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(InetAddress ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	

	


}
