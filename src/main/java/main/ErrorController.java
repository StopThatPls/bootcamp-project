package main;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import main.exceptions.ConflictException;
import main.exceptions.NotFoundException;

@ControllerAdvice
public class ErrorController {

	@ExceptionHandler(NotFoundException.class)
	public ModelAndView notFoundExceptionHandler(NotFoundException e, HttpServletResponse response) {
		response.setStatus(404);
		ModelAndView model = new ModelAndView("error");
		String errorMessage = e.getMessage();
		model.addObject("errorHeader", "Error 404: Welcome to Narnia");
		model.addObject("errorClass", "err404");
		model.addObject("errorMessage", errorMessage);
		return model;
	}

	@ExceptionHandler(ConflictException.class)
	public ModelAndView conflictExceptionHandler(ConflictException e, HttpServletResponse response) {
		response.setStatus(409);
		ModelAndView model = new ModelAndView("error");
		String errorMessage = e.getMessage();
		model.addObject("errorHeader", "Error 409: Conflict happened");
		model.addObject("errorClass", "err409");
		model.addObject("errorMessage", errorMessage);
		return model;
	}

	@ExceptionHandler(RuntimeException.class)
	public ModelAndView runtimeExceptionHandler(RuntimeException e, HttpServletResponse response) {
		response.setStatus(500);
		ModelAndView model = new ModelAndView("error");
		String errorMessage = e.getMessage();
		model.addObject("errorMessage",
				errorMessage.length() > 0 ? errorMessage : "And even we don't know what exactly...");
		return model;
	}
}
