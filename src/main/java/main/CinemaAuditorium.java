package main;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "cinema_auditorium")
public class CinemaAuditorium {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	private Integer cinemaId;
	
	private String name;
	
	private Integer rows;
	
	private Integer seatsPerRow;
	private boolean active;
	
	public CinemaAuditorium() {
		
	}
	
	public CinemaAuditorium(Integer cinemaId, String name, Integer row, Integer seatsPerRow) {
//		this.cinemaId = cinemaId;
//		this.name = name;
//
//		this.seatsPerRow = row;
//
//		this.rows = row;
//
//		this.seatsPerRow = seatsPerRow;
		setCinemaId(cinemaId);
		setName(name);
		setRows(row);
		setSeatsPerRow(seatsPerRow);

		this.active = true;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public Integer getCinemaId() {
		return cinemaId;
	}
	
	public void setCinemaId(Integer cinemaId) {
		if(cinemaId!=null && cinemaId>0) {
			this.cinemaId = cinemaId;
		}
		else {
			this.cinemaId = 1;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name != null && !name.isEmpty()) {
			this.name = name;
		}else {
			this.name = "Not set";
		}
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		if(rows != null && rows > 0) {
			this.rows = rows;
		}
		else {
			this.rows = 1;
		}
		
	}
	
	public Integer getSeatsPerRow() {
		return seatsPerRow;
	}
	
	public void setSeatsPerRow(Integer seatsPerRow) {
		
		if(seatsPerRow != null && seatsPerRow > 0) {
			this.seatsPerRow = seatsPerRow;
		}
		else {
			this.seatsPerRow = 1;
		}
		
	}
	
	public static String[] getKeys(){
		return new String[] {"id", "cinemaId", "name","rows","seatsPerRow"};
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	
}
