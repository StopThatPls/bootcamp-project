package main;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import main.CinemaSchedule;
import main.ScheduleRepository;



@Controller    
@RequestMapping(path="/cinema/{cinemaId}/movies")  //
public class ScheduleController {
		@Autowired 
		private ScheduleRepository scheduleRepository;

		@Autowired 
		private MovieRepository movieRepository;
		
		@Autowired 
		private CinemaRepository cinemaRepository;
		
		private static final Logger log = LoggerFactory.getLogger(CinemaBookingSystemApplication.class);
		
		/**ADD SCHEDULE**/
		
		@GetMapping(path="/addschedule") 
		public @ResponseBody String addNewSchedule (@RequestParam Timestamp dateTime, @RequestParam Integer cinemaId, @RequestParam Integer movieId, @RequestParam Integer auditoriumId
		) {
		return "Schedule saved";
		}
		
		
		/**SHOWS SCHEDULE FOR SPECIFIC MOVIE**/
		
		@GetMapping("/{movieId}")
		String readCinemaSchedule(@PathVariable long cinemaId, @PathVariable long movieId, Model model) {
			Optional<Movie> movieSearch = movieRepository.findById(movieId);
			Optional<Cinema> cinemaSearch = cinemaRepository.findById(cinemaId);
			
			if (cinemaSearch.isPresent() && movieSearch.isPresent()) {
				Cinema cinema = cinemaSearch.get();
				Movie movie = movieSearch.get();
				List<CinemaSchedule> movieSearchResults = scheduleRepository.findByCinemaIdAndMovieId(cinemaId, movieId);
								
				model.addAttribute("cinemaName", cinema.getName());
				model.addAttribute("movie", movie);
				model.addAttribute("schedules", movieSearchResults);
				
				return  "fragments/schedule/schedule"; 
			} else {
				throw new RuntimeException("Cinema/Movie not found!");
			}
		}
		

		
		@GetMapping(path="/schedule")
		public @ResponseBody Iterable<CinemaSchedule> getAllSchedule() {
		return scheduleRepository.findAll();
	}
}