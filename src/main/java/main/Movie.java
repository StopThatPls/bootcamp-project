package main;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "movie")
public class Movie {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private String name;
	@Column(columnDefinition = "text")
	private String description;
	private String posterLink;
	private String trailerLink;
	private boolean active;

	Movie() {
	} // JPA only

	public Movie(String name, String description, String posterLink, String trailerLink) {
		setName(name);
		setDescription(description);
		setPosterLink(posterLink);
		setTrailerLink(trailerLink);
		setActive(true);
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		
		if (name != null && !name.isEmpty()) {
			this.name = name;
		}else {
			this.name = "Not set";
		}
		
	}

	public String getPosterLink() {
		return posterLink;
	}

	public void setPosterLink(String posterLink) {
		if (posterLink != null && !posterLink.isEmpty()) {
			this.posterLink = posterLink;
		}else {
			this.posterLink = "Not set";
		}
	}

	public String getTrailerLink() {
		return trailerLink;
	}

	public void setTrailerLink(String trailerLink) {
		if (trailerLink != null && !trailerLink.isEmpty()) {
			this.trailerLink = trailerLink;
		}else {
			this.trailerLink = "Not set";
		}
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (description != null && !description.isEmpty()) {
			this.description = description;
		}else {
			this.description = "Not set";
		}
	}
	public static String[] getKeys(){
		return new String[] {"id", "name", "description","posterLink","trailerLink"};
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}