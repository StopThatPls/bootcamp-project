package main;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import main.UserService;
//import com.example.model.Role;
import main.User;
//import main.RoleRepository;
import main.UserRepository;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	// @Autowired
	// private RoleRepository roleRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public Optional<User> findUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public void saveUser(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setRole('u');


		user.setActive(true);
        //user.setActive(1);
        //Role userRole = roleRepository.findByRole("ADMIN");
        //user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));

		// user.setActive(1);
		// Role userRole = roleRepository.findByRole("ADMIN");
		// user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));


		// user.setActive(1);
		// Role userRole = roleRepository.findByRole("ADMIN");
		// user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));

		userRepository.save(user);
	}

//	public static Optional<User>  getCurrentUser () {
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		return userRepository.findByEmail(auth.getName());
//	}

}
