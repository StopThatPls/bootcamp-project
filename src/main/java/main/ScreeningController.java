package main;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import main.exceptions.NotFoundException;
import main.forms.PurchaseForm;

@Controller
@RequestMapping(path = "cinema/{cinemaId}/screening")
public class ScreeningController {

	@Autowired
	private ScheduleRepository scheduleRepository;
	@Autowired
	private SeatReservationRepository seatReservationRepository;

	@GetMapping(path = "/{screeningId}")
	public String getScreening(@PathVariable Long cinemaId, @PathVariable Long screeningId, PurchaseForm purchaseForm,
			Model model) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!auth.isAuthenticated() || auth.getName().equals("anonymousUser")) {
			return "redirect:/login";
		}

		Optional<CinemaSchedule> scheduleSearchResult = scheduleRepository.findById(screeningId);
		if (scheduleSearchResult.isPresent()) {
			CinemaSchedule schedule = scheduleSearchResult.get();
			if (schedule.getCinema().getId() != cinemaId) {
				throw new NotFoundException("Show time belongs to another cinema. How did you even get here?");
			}
			model.addAttribute("movieName", schedule.getMovie().getName());
			Date dateTime = new Date(schedule.getTimeFrom().getTime());
			String formattedDateTime = new SimpleDateFormat("MMMM d, yyyy - H:mm").format(dateTime);
			purchaseForm.setScheduleId(schedule.getId());
			model.addAttribute("dateTime", formattedDateTime);
			model.addAttribute("scheduleId", schedule.getId());
			List<List<Seat>> auditoriumLayout = new ArrayList<>();
			for (int i = schedule.getAuditorium().getRows(); i >= 1; i--) {
				List<Seat> rowLayout = new ArrayList<>();
				for (int j = 1; j <= schedule.getAuditorium().getSeatsPerRow(); j++) {
					Seat seat = new Seat(i, j);
					Optional<SeatReservation> seatSearch = seatReservationRepository.findByScreeningAndSeat(schedule,
							seat);
					if (seatSearch.isPresent()) {
						seat.setOccupied(true);
					}
					rowLayout.add(seat);
				}
				auditoriumLayout.add(rowLayout);
			}
			model.addAttribute("auditoriumLayout", auditoriumLayout);
			return "fragments/screening/show";
		} else {
			throw new NotFoundException("Show time doesn't exist.");
		}
	}
}