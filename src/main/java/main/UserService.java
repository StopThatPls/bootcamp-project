package main;

import java.util.Optional;

import main.User;

public interface UserService {
	public Optional<User> findUserByEmail(String email);
	public void saveUser(User user);
}
