package main;

import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import main.User;

public interface UserRepository extends CrudRepository<User, Long>{
	User findByName(String name);
	User findByNameAndPassword(String name, String password);
	Optional<User> findByEmail(String email);
	
	
	@Transactional
	@Modifying
    @Query("update user u set u.email = ?1, u.name = ?2,u.role=?3 where u.id = ?4")
    void updateUser(String email, String name, char role, long id);
	
	
	@Transactional
	@Modifying
    @Query("update user u set u.active = ?1 where u.id = ?2")
    void updateUserActive(boolean active, long id);
	
}
