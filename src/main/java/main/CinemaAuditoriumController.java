package main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import main.CinemaAuditorium;
import main.CinemaAuditoriumRepository;

	@Controller
	@RequestMapping(path="/demo2")
	public class CinemaAuditoriumController {
		@Autowired
		
		private CinemaAuditoriumRepository cinemaAudRep;
		
		@GetMapping(path="/add")
		public @ResponseBody String addNewAuditorium(@RequestParam String name, @RequestParam int cinemaId, 
				@RequestParam int row, @RequestParam int seatsPerRow) {
			CinemaAuditorium cinemaAud = new CinemaAuditorium();
			Cinema cinema = new Cinema();
			cinemaAud.setName(name);
			cinema.getId();
			cinemaAud.setRows(row);
			cinemaAud.setSeatsPerRow(seatsPerRow);
			cinemaAudRep.save(cinemaAud);
			Log.logger.debug("Auditorium with id: " + cinemaAud.getId() + "has been added for cinema with id: " + cinema.getId());
			return "Information has been saved";
		}
		
		@GetMapping(path="/all")
		public @ResponseBody Iterable<CinemaAuditorium> getAllAuditoriums(){
			return cinemaAudRep.findAll();
		}

}
