
package main;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import junit.framework.Assert;

@SpringBootTest(classes = CinemaBookingSystemApplicationTests.class)
@EnableJpaRepositories(basePackages = "CinemaRepository")
public class CinemaBookingSystemApplicationTests {

	
    

	//Cinema testing
	
	
	
	@Test
	public void createCinema() {
		String name = "Cinema";
		String location = "London";
		String address = "Something street";
		String imgUrl = "picture.jpg";
		
		
		Cinema cin = new Cinema(name,location,address,imgUrl);
		Log.logger.debug("Create cinema Test failed");
		assertEquals("Name does not match the given parameter",name,cin.getName());
		assertEquals("Location does not match the given parameter",location,cin.getCity());
		assertEquals("Address does not match the given parameter",address,cin.getAddress());
		assertEquals("Image does not match the given parameters",imgUrl,cin.getImgUrl());
	}
	
	@Test
	public void createCinemaWithEmptyValues() {
		String name = "";
		String city = "";
		String address = "";
		String imgUrl = "";
		Cinema cin = new Cinema(name,city,address,imgUrl);
		Log.logger.debug("Cinema with empty values Test failed");
		assertFalse("Adding empty name should not be possible",name.equals(cin.getName()));
		assertFalse("Adding empty city should not be possible",city.equals(cin.getCity()));
		assertFalse("Adding empty address should not be possible",address.equals(cin.getAddress()));
		assertFalse("Adding empty Image should not be possible",imgUrl.equals(cin.getImgUrl()));
	}
	
	
	 
	@Test
	public void createMovie() {
		String name = "SpiderMan";
		String description = "Spiderman dies";
		String posterLink = "imageSpider.jpg";
		String trailerLink = "https://youtube.com";
		Movie mov = new Movie(name,description,posterLink,trailerLink);
		Log.logger.debug("Create movie Test failed");
		assertEquals("Name does not match the given parameter",name,mov.getName());
		assertEquals("Description does not match the given parameter",description,mov.getDescription());
		assertEquals("Poster link does not match the given parameter",posterLink,mov.getPosterLink());
		assertEquals("Trailer link does not match the given parameter",trailerLink,mov.getTrailerLink());	
	}
	
//	@Test
//	public void createMovieLinkCheck() {
//		String name = "SpiderMan";
//		String description = "Spiderman dies";
//		String posterLink = "asdasdasd";
//		String trailerLink = "yutub";
//		
//		Movie mov = new Movie(name,description,posterLink,trailerLink);
//		
//		assertFalse("PosterLink should be a proper image link, that exists",posterLink.equals(mov.getPosterLink()));
//		assertFalse("Adding empty address should not be possible",trailerLink.equals(mov.getTrailerLink()));
//	}
//	
	
	/** Cinema test with null values**/
	@Test
	public void createCinemaWithNull() {
		String name = null;
		String city = null;
		String address = null;
		String imgUrl = null;
		Cinema cinema = new Cinema(name,city,address,imgUrl);
		Log.logger.debug("Create cinema with null Test failed");
		assertNotNull("Cinema name cant be null",cinema.getName());
		assertNotNull("Cinema address cant be null",cinema.getAddress());
		assertNotNull("Cinema city cant be null",cinema.getCity());
		assertNotNull("Cinema imgUrl cant be null",cinema.getImgUrl());
	}
	
	
	/** Movie test with null values **/
	@Test
	public void createMovieWithNull() {
		String name = null;
		String description = null;
		String posterLink = null;
		String trailerLink = null;
		Movie movie = new Movie(name,description,posterLink,trailerLink);
		Log.logger.debug("Create movie with null Test failed");
		assertNotNull("Movie name cant be null",movie.getName());
		assertNotNull("Movie description cant be null",movie.getDescription());
		assertNotNull("Movie poster link cant be null",movie.getPosterLink());
		assertNotNull("Movie trailer link cant be null",movie.getTrailerLink());
	}
	
	/** Movie test with values **/
	@Test 
	public void createMovieWithValues() {
		String nameData = "Best movie";
		String descriptionData = "Good movie";
		String posterLinkData = "niceimage.jpg";
		String trailerLinkData = "https:youtube/bestmovie";
		Movie movieData = new Movie(nameData,descriptionData,posterLinkData,trailerLinkData);
		Log.logger.debug("Create movie with values Test failed");
		assertEquals("Name does not match the given parameter",nameData,movieData.getName());
		assertEquals("Description does not match the given parameter",descriptionData,movieData.getDescription());
		assertEquals("Poster link does not match the given parameter",posterLinkData,movieData.getPosterLink());
		assertEquals("Trailer link does not match the given parameter",trailerLinkData,movieData.getTrailerLink());
	}
	
	/** Movie test with empty values **/
	@Test
	public void createMovieWithEmptyValues() {
		String name = "";
		String description = "";
		String posterLink = "";
		String trailerLink = "";
		Movie movie = new Movie(name,description,posterLink,trailerLink);
		Log.logger.debug("Create movie with empty values Test failed");
		assertFalse("Adding empty name should not be possible",name.equals(movie.getName()));
		assertFalse("Adding empty description should not be possible",description.equals(movie.getDescription()));
		assertFalse("Adding empty posterLink should not be possible",posterLink.equals(movie.getPosterLink()));
		assertFalse("Adding empty trailerLink should not be possible",trailerLink.equals(movie.getTrailerLink()));
	}
	
	
	/** Auditorium with values **/
	@Test
	public void createAuditoriumWithValues() {
		Integer cinemaId = 1;
		String name = "Best";
		Integer rows = 2;
		Integer seatsPerRow = 10;
		CinemaAuditorium auditorium = new CinemaAuditorium(cinemaId,name,rows,seatsPerRow);
		Log.logger.debug("Create auditorium with values Test failed");
		assertEquals("CinemaId does not match the given parameter",cinemaId,auditorium.getCinemaId());
		assertEquals("Auditroium name does not match the given parameter",name,auditorium.getName());
		assertEquals("Auditorium rows does not match the given parameter",rows,auditorium.getRows());
		assertEquals("Auditorium seats per row does not match the given parameter",seatsPerRow,auditorium.getSeatsPerRow());
	}
	/** Auditorium with null values **/
	@Test
	public void createAuditoriumWithNull() {
		Integer cinemaId = null;
		String name = null;
		Integer rows = null;
		Integer seatsPerRow = null;
		CinemaAuditorium auditorium = new CinemaAuditorium(cinemaId,name,rows,seatsPerRow);
		Log.logger.debug("Create auditorium with null Test failed");
		assertNotNull("CinemaId cant be null",auditorium.getCinemaId());
		assertNotNull("Auditorium name cant be null",auditorium.getName());
		assertNotNull("Auditorium rows poster link cant be null",auditorium.getRows());
		assertNotNull("Auditorium seatsPerRow link cant be null",auditorium.getSeatsPerRow());
	}
	/** Auditorium with wrong values **/
	@Test
	public void createAuditoriumWithWrongValues() {
		
			Integer cinemaId = -1;
			String name = "";
			Integer rows = -1;
			Integer seatsPerRow = -10;
			CinemaAuditorium auditorium = new CinemaAuditorium(cinemaId,name,rows,seatsPerRow);
			Log.logger.debug("Create auditorium with wrong values Test failed");
			assertFalse("Adding negative cinemaId should not be possible",auditorium.equals(auditorium.getCinemaId()));
			assertFalse("Adding empty auditroium name should not be possible",auditorium.equals(auditorium.getName()));
			assertFalse("Adding negative rows should not be possible",auditorium.equals(auditorium.getRows()));
			assertFalse("Adding negative seatsPerRow should not be possible",auditorium.equals(auditorium.getSeatsPerRow()));
	}
	
	
	/** Cinema schedule**/
	
	@Test
	public void createCinemaSchedule() {
		
		Timestamp fromTime = Timestamp.valueOf("2018-06-19 17:30:00");
		Timestamp toTime = Timestamp.valueOf("2018-06-19 17:25:00");
		
		Cinema cinema = new Cinema(null,"vpils","streeet","link");
		//Cinema cinema = new Cinema();
		CinemaAuditorium auditorium = new CinemaAuditorium(1,null,10,15);
		//Movie movie = new Movie("Random movie3","good movie","posterlinks","trailerlinks");
		Movie movie = new Movie(null,"good movie","posterlinks","trailerlinks");
		
		
		
		CinemaSchedule schedule = new CinemaSchedule(fromTime,toTime,auditorium,cinema,movie);
		
		//System.out.println(fromTime.toString());
		//System.out.println(toTime.toString());
		
		
		//Movie
		assertNotNull("Movie name cant be null",schedule.getMovie().getName());
		assertEquals(movie,schedule.getMovie());
		assertNotNull("Movie object cant be null",movie);
				
		
		//Cinema
		assertEquals(cinema,schedule.getCinema());
		assertNotNull("Cinema name cant be null",schedule.getCinema().getName());
		assertNotNull("Cinema adress cant be null",schedule.getCinema().getAddress());
		assertNotNull("Cinema city cant be null",schedule.getCinema().getCity());
		assertNotNull("Auditorium name cant be null",schedule.getAuditorium().getName());
		
		
		//Auditorium
		assertNotNull("Cinema object cant be null",cinema);
		assertEquals(auditorium,schedule.getAuditorium());
		assertNotNull("Auditorium object cant be null",auditorium);
		
		
	}
	/* Purchases 
	 * Purchase(int userId,
	 * int scheduleId,
	 * int rowNumber,
	 * int seatNumber,
	 * double ticketCost,
	 * char status)
	 */
	
//	@Test
//	public void createPurchase() {
//		int userId = 1;
//		int scheduleId = 1;
//		int rowNumber = 1;
//		int seatNumber = 7;
//		double ticketCost = 3.50;
//		char status = 'p';
//		Purchase pur = new Purchase(userId,scheduleId,rowNumber,seatNumber,ticketCost,status);
//		
//	}
//	@Test
//	public void createPurchaseWithNegative() {
//		Integer userId = -1;
//		Integer scheduleId = -1;
//		Integer rowNumber = -1;
//		Integer seatNumber = -7;
//		Double ticketCost = -3.50;
//		Character status = 'a';
//		Purchase p = new Purchase(userId,scheduleId,rowNumber,seatNumber,ticketCost,status);
//		assertFalse("User ID should not be negative",userId.equals(p.getUserId()));
//		assertFalse("Schedule ID should not be negative",scheduleId.equals(p.getScheduleId()));
//		assertFalse("Row Number should not be negative",rowNumber.equals(p.getRowNumber()));
//		assertFalse("Seat Number should not be negative",seatNumber.equals(p.getSeatNumber()));
//		assertFalse("Ticket cost should not be negative",ticketCost.equals(p.getTicketCost()));
//		assertFalse("Status should be either 'p' or 'd'",status.equals(p.getStatus()));
//	}

	
	
	
	/*
	cinema testing
		//-If creates propely
		-test if can input unnecessary things
	movie testing
		//-If creates properly
		-if can test unnecessary inputs
	purchase testing
		- if seat/row number can be negative
		- if char can be something other than specified ones
		- if can add with non existant userId (needs implemented)
		- if can add with non existant scheduleId(Needs implemented)
	Screening testing 
		I don't know how this works for now, postponed
	Auditorium testing
		- if can add negative row/column
		- if creates properly 
		- if can create with non-existant cinema(if needs to be implemented)
		
	*/
	
	

}
