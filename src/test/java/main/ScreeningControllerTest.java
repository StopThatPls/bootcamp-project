package main;

import static org.hamcrest.CoreMatchers.containsString;

import java.sql.Timestamp;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@WebMvcTest(ScreeningController.class)
public class ScreeningControllerTest {

	@Configuration
	public static class TestConfiguration {

		@Bean
		public ScreeningController screeningController() {
			return new ScreeningController();
		}

	}

	@MockBean
	private ScheduleRepository scheduleRepository;
	@MockBean
	private SeatReservationRepository seatReservationRepository;

	@Autowired
	private MockMvc mockMvc;

	@Before
	public void setUp() {
		Cinema cinema = new Cinema("Str", "Str", "Str", "Str");
		cinema.setId(1);
		CinemaSchedule sched = new CinemaSchedule(Timestamp.valueOf("2018-07-20 16:30:00"),
				Timestamp.valueOf("2018-07-20 18:30:00"), new CinemaAuditorium(1, "Aud", 8, 16), cinema,
				new Movie("Str", "Str", "Str", "Str"));
		Mockito.when(scheduleRepository.findById((long) 1)).thenReturn(Optional.of(sched));
	}

	@Test
	public void testGetScreening_notFound() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/cinema/1/screening/42"))
				.andExpect(MockMvcResultMatchers.status().is(404));
	}

	@Test
	public void testGetScreening_found() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/cinema/1/screening/1"))
				.andExpect(MockMvcResultMatchers.status().is(200))
				.andExpect(MockMvcResultMatchers.content().string(containsString("reservationForm")));
	}

}
