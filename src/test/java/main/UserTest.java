package main;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import junit.framework.Assert;

@SpringBootTest(classes = UserTest.class)
@EnableJpaRepositories(basePackages = "UserRepository")
public class UserTest {
	
	// User test
	User user = new User();
	
	@Test
	public void createUser() {
		
		String name = "John";
		String email = "john@email.com";
		String password = "pass12!@";
		Character role = 'A';
		
		User user = new User(name, email, password, role);
		
		assertEquals("Name does not match the given parameter", name, user.getName());
		assertEquals("Email does not match the given parameter", email, user.getEmail());
		assertEquals("Password does not match the given parameter", password, user.getPassword());
		//assertEquals("Role does not match the given parameter", role, user.getRole());
	}
	
	@Test
	public void createUserWithEmptyValues() {
		
		String name = "";
		String email = "";
		String password = "";
		Character role = ' ';
		
		assertFalse("Adding empty name should not be possible", name.equals(user.getName()));
		assertFalse("Adding empty city should not be possible", email.equals(user.getEmail()));
		assertFalse("Adding empty address should not be possible", password.equals(user.getPassword()));
		//assertFalse("Adding empty address should not be possible", role.equals(user.getRole()));
	}
	
	@Test
	public void checkIfStringIsNotEmpty() {
		
		String name = "123abc";
		user.setName(name);
		
		assertTrue(!name.isEmpty());
		assertFalse(name.isEmpty());
	}		
}
